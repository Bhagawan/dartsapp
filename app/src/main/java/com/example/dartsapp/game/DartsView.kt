package com.example.dartsapp.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.dartsapp.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.*
import kotlin.random.Random

class DartsView(context: Context, attributeSet: AttributeSet?) : View(context, attributeSet) {
    private var mWidth = 0
    private var mHeight = 0
    private var dartSpeed = 10

    private val target = BitmapFactory.decodeResource(context.resources, R.drawable.target)
    private val restart = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_replay)?.toBitmap() ?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)
    private val dart = BitmapFactory.decodeResource(context.resources, R.drawable.dart)

    private var state = GAME
    private var score = 500
    private var scoreFlash = 0

    private val darts = ArrayList<DartState>()
    private var dartFloating = false
    private var dartPosition = DartState(0,0, 0.0f, 1.0f, 1.0f)
    private var dartDX = 0
    private var dartDY = 0
    private var dartScaleDX = 0.0f
    private var dartScaleDY = 0.0f

    private var hitNotificationTimer = 0
    private var notification : HitNotification? = null

    companion object {
        const val GAME = 0
        const val WIN_SCREEN = 1
    }

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            dartSpeed =( min(mWidth, mHeight) * 0.04f).toInt()
            if(!dartFloating) dartPosition = DartState(mWidth / 2, mHeight - dart.height / 2, 180f, 1.0f,0.5f)
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            drawTarget(it)
            drawUI(it)
            if(dartFloating) updateDart()
            if(state == GAME) drawDart(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                when(state) {
                    GAME -> {

                    }
                    WIN_SCREEN -> {

                    }
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                when(state) {
                    GAME -> {

                    }
                    WIN_SCREEN -> {

                    }
                }
                return true
            }
            MotionEvent.ACTION_UP -> {
                when(state) {
                    GAME -> {
                        if(checkRestartPush(event.x, event.y)) restart()
                        else if(!dartFloating) trowDartTo(event.x, event.y)
                    }
                    WIN_SCREEN -> {
                        restart()
                    }
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    private fun drawTarget(c: Canvas) {
        val p = Paint()

        val radius : Int = min(mWidth, mHeight) / 5 * 2
        val targetRect = Rect(mWidth / 2 - radius, mHeight / 2 - radius, mWidth / 2 + radius, mHeight / 2 + radius)

        c.drawBitmap(target, null, targetRect, p)

        for(d in darts) {
            val transformMatrix = Matrix()
            transformMatrix.postScale(d.scaleX, d.scaleY)
            transformMatrix.postRotate(d.angle)
            val transformedDart = Bitmap.createBitmap(dart, 0,0, dart.width, dart.height, transformMatrix, true)

            val dx = when(d.angle.toInt() % 360) {
                0 -> dart.width * d.scaleX * 0.5f
                in 1..89 -> (dart.width * d.scaleX * cos(Math.toRadians(d.angle.toDouble() % 90))/ 2.0f).toFloat()
                90 -> 0.0f
                in 91..179 -> (dart.width * d.scaleX * sin(Math.toRadians(d.angle.toDouble() % 90))/ 2.0f).toFloat()
                180 -> dart.width * d.scaleX * 0.5f
                in 181..269 ->  transformedDart.width - (dart.width * d.scaleX * cos(Math.toRadians(d.angle.toDouble() % 90))/ 2.0f).toFloat()
                270 -> transformedDart.width.toFloat()
                else ->  transformedDart.width - (dart.width * d.scaleX * sin(Math.toRadians(d.angle.toDouble() % 90))/ 2.0f).toFloat()
            }

            val dy = when(d.angle.toInt() % 360) {
                0 -> 0.0f
                in 1..89 -> (dart.width * d.scaleX * sin(Math.toRadians(d.angle.toDouble() % 90))/ 2.0f).toFloat()
                90 -> dart.width * d.scaleX * 0.5f
                in 91..179 -> transformedDart.height - (dart.width * d.scaleX * cos(Math.toRadians(d.angle.toDouble() % 90))/ 2.0f).toFloat()
                180 -> transformedDart.height.toFloat()
                in 181..269 ->  transformedDart.height - (dart.width * d.scaleX * sin(Math.toRadians(d.angle.toDouble() % 90))/ 2.0f).toFloat()
                270 -> dart.width * d.scaleX * 0.5f
                else ->  (dart.width * d.scaleX * cos(Math.toRadians(d.angle.toDouble() % 90))/ 2.0f).toFloat()
            }
            c.drawBitmap(transformedDart, mWidth / 2.0f + d.xFromMiddle - dx,
                mHeight / 2.0f - transformedDart.height + d.yFromMiddle + dy, p)
        }
    }

    private fun drawUI(c: Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.strokeWidth = 3.0f
        p.textAlign = Paint.Align.CENTER
        p.color = ResourcesCompat.getColor(context.resources, R.color.grey, null)

        when(state) {
            GAME -> {
                p.textSize = 40.0f
                val scoreX : Float
                val scoreY : Float
                val tryX : Float
                val tryY : Float
                if(mWidth > mHeight) {
                    scoreX = (mWidth / 2 - mHeight * 0.4f) / 2 - (score.toString().length * 30) / 2.0f
                    scoreY = mHeight / 3.0f
                    tryX = (mWidth / 2 - mHeight * 0.4f) / 2
                    tryY = mHeight / 2.0f
                } else {
                    scoreX = mWidth / 2.0f - (score.toString().length * 30) / 2.0f
                    scoreY = 50.0f
                    tryX = mWidth / 2.0f
                    tryY =  scoreY + 70
                }
                p.color = if(scoreFlash > 0) {
                    scoreFlash--
                    ResourcesCompat.getColor(context.resources, R.color.red, null)
                } else ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)

                p.style = Paint.Style.FILL
                c.drawRoundRect(scoreX, scoreY - 50, scoreX + (score.toString().length * 30) + 10, scoreY, 20f, 20f, p)
                p.color = Color.WHITE
                p.style = Paint.Style.STROKE
                c.drawRoundRect(scoreX, scoreY - 50, scoreX + (score.toString().length * 30) + 10, scoreY, 20f, 20f, p)
                p.style = Paint.Style.FILL
                c.drawText(score.toString(), scoreX + (score.toString().length * 30) / 2.0f + 5, scoreY - 10, p)

                c.drawText("Попыток:", tryX, tryY, p)
                c.drawText(darts.size.toString(), tryX, tryY + 40, p)

                val restartPadding = 20
                val restartSize = min(mWidth, mHeight) * 0.1f
                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
                c.drawRoundRect(mWidth - restartSize - restartPadding - 10,
                    mHeight - restartSize - restartPadding - 10,
                    mWidth - restartPadding + 10.0f, mHeight - restartPadding + 10.0f, 20f, 20f, p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey, null)
                p.style = Paint.Style.STROKE
                c.drawRoundRect(mWidth - restartSize - restartPadding - 10,
                    mHeight - restartSize - restartPadding - 10,
                    mWidth - restartPadding + 10.0f, mHeight - restartPadding + 10.0f, 20f, 20f, p)
                c.drawBitmap(restart, null, Rect((mWidth - restartSize - restartPadding).toInt(),
                    (mHeight - restartSize - restartPadding).toInt(), mWidth - restartPadding, mHeight - restartPadding), p)

                if(hitNotificationTimer > 0) {
                    hitNotificationTimer--
                    showHitNotification(c)
                }

            }
            WIN_SCREEN -> {
                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
                c.drawRoundRect(mWidth / 4.0f,
                    mHeight * 0.25f,
                    mWidth * 0.75f, mHeight * 0.75f, 20f, 20f, p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey, null)
                p.style = Paint.Style.STROKE
                c.drawRoundRect(mWidth / 4.0f,
                    mHeight * 0.25f,
                    mWidth * 0.75f, mHeight * 0.75f, 20f, 20f, p)

                p.textSize = 40.0f
                p.style = Paint.Style.FILL
                p.color = Color.YELLOW
                c.drawText("Победа!", mWidth / 2.0f, mHeight * 0.3f, p)
                p.textSize = 30.0f
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey, null)
                c.drawText("Количество попыток:", mWidth / 2.0f, mHeight * 0.3f + 30, p)
                p.textSize = 60.0f
                c.drawText("${darts.size}", mWidth / 2.0f, mHeight * 0.3f + 60, p)

                val restartSize = min(mWidth, mHeight) * 0.3f

                c.drawBitmap(restart, null, Rect(((mWidth - restartSize) / 2).toInt(),
                    (mHeight * 0.75f - restartSize - 10).toInt(),
                    ((mWidth + restartSize) / 2).toInt(), (mHeight * 0.75f - 10).toInt()), p)
            }
        }
    }

    private fun drawDart(c: Canvas) {
        val transformMatrix = Matrix()
        transformMatrix.postScale(dartPosition.scaleX.absoluteValue, if(dartPosition.scaleY.absoluteValue < 0.01 ) 0.01f else dartPosition.scaleY)
        transformMatrix.postRotate(dartPosition.angle)
        val transformedDart = Bitmap.createBitmap(dart, 0,0, dart.width, dart.height, transformMatrix, true)
        val dx = when(dartPosition.angle.toInt() % 360) {
            0 -> dart.width * dartPosition.scaleX * 0.5f
            in 1..89 -> (dart.width * dartPosition.scaleX * cos(Math.toRadians(dartPosition.angle.toDouble() % 90))/ 2.0f).toFloat()
            90 -> 0.0f
            in 91..179 -> (dart.width * dartPosition.scaleX * sin(Math.toRadians(dartPosition.angle.toDouble() % 90))/ 2.0f).toFloat()
            180 -> dart.width * dartPosition.scaleX * 0.5f
            in 181..269 ->  transformedDart.width - (dart.width * dartPosition.scaleX * cos(Math.toRadians(dartPosition.angle.toDouble() % 90))/ 2.0f).toFloat()
            270 -> transformedDart.width.toFloat()
            else ->  transformedDart.width - (dart.width * dartPosition.scaleX * sin(Math.toRadians(dartPosition.angle.toDouble() % 90))/ 2.0f).toFloat()
        }

        val dy = when(dartPosition.angle.toInt() % 360) {
            0 -> 0.0f
            in 1..89 -> (dart.width * dartPosition.scaleX * sin(Math.toRadians(dartPosition.angle.toDouble() % 90))/ 2.0f).toFloat()
            90 -> dart.width * dartPosition.scaleX * 0.5f
            in 91..179 -> transformedDart.height - (dart.width * dartPosition.scaleX * cos(Math.toRadians(dartPosition.angle.toDouble() % 90))/ 2.0f).toFloat()
            180 -> transformedDart.height.toFloat()
            in 181..269 ->  transformedDart.height - (dart.width * dartPosition.scaleX * sin(Math.toRadians(dartPosition.angle.toDouble() % 90))/ 2.0f).toFloat()
            270 -> dart.width * dartPosition.scaleX * 0.5f
            else ->  (dart.width * dartPosition.scaleX * cos(Math.toRadians(dartPosition.angle.toDouble() % 90))/ 2.0f).toFloat()
        }
        c.drawBitmap(transformedDart, dartPosition.xFromMiddle - dx,
            dartPosition.yFromMiddle - transformedDart.height + dy, Paint()
        )
    }

    private fun updateDart() {
        dartPosition.scaleX -= dartScaleDX
        dartPosition.scaleY -= dartScaleDY

        dartPosition.xFromMiddle += dartDX
        if(dartPosition.scaleY > 0)  dartPosition.yFromMiddle += dartDY
        else dartPosition.yFromMiddle -= dartDY

        if(dartPosition.scaleX <= 0.3f) {
            val a =  Random.nextInt(360).toFloat()
            darts.add(DartState(dartPosition.xFromMiddle - mWidth / 2,
                 dartPosition.yFromMiddle - mHeight / 2,
                a, 0.3f, 0.1f))
            landDart(dartPosition.xFromMiddle, dartPosition.yFromMiddle)
            dartPosition = DartState(mWidth / 2 , mHeight - dart.height / 2, 180f, 1.0f,0.5f)
            dartFloating = false
        }
    }

    private fun trowDartTo(x: Float, y: Float) {
        dartScaleDX = 0.7f / dartSpeed
        dartScaleDY = 0.6f / dartSpeed
        dartDX = (x - dartPosition.xFromMiddle).toInt() / dartSpeed
        dartDY = ((y - dartPosition.yFromMiddle) * 1.7f).toInt() / dartSpeed
        dartPosition.angle = 180 - Math.toDegrees(atan((x - dartPosition.xFromMiddle) / (y - dartPosition.yFromMiddle)).toDouble()).toFloat()
        dartFloating = true
    }

    private fun landDart(x: Int, y: Int) {
        val distance = sqrt((x - mWidth / 2.0f).pow(2) + (y - mHeight / 2.0f).pow(2))
        val radius : Int = min(mWidth, mHeight) / 5 * 2
        when(distance) {
            in 0.0f..(radius / 315.0f * 10.0f) ->  {
                hitNotificationTimer = 15
                notification = HitNotification(100, HitNotification.Companion.HitType.MIDDLE)
                if(score > 100) score -= 100
                else if(score == 100) state = WIN_SCREEN
                else scoreFlash = 20
            }
            in (radius / 315.0f * 10.0f)..(radius / 315.0f * 24.0f) ->  {
                hitNotificationTimer = 15
                notification = HitNotification(50, HitNotification.Companion.HitType.MIDDLE)

                if(score > 50) score -= 50
                else if(score == 50) state = WIN_SCREEN
                else scoreFlash = 20
            }
            in (radius / 315.0f * 24.0f)..(radius / 315.0f * 145.0f) ->  {
                hitNotificationTimer = 15
                val p = getPoints(x, y)
                notification = HitNotification(p, HitNotification.Companion.HitType.ORDINARY)
                if(score > p) score -= p
                else if(score == p) state = WIN_SCREEN
                else scoreFlash = 20
            }
            in (radius / 315.0f * 145.0f)..(radius / 315.0f * 160.0f) ->  {
                hitNotificationTimer = 15
                val p = getPoints(x, y)
                notification = HitNotification(p, HitNotification.Companion.HitType.TRIPLE)
                if(score > p) score -= p
                else if(score == p) state = WIN_SCREEN
                else scoreFlash = 20
            }
            in (radius / 315.0f * 160.0f)..(radius / 315.0f * 240.0f) ->  {
                hitNotificationTimer = 15
                val p = getPoints(x, y)
                notification = HitNotification(p, HitNotification.Companion.HitType.ORDINARY)
                if(score > p) score -= p
                else if(score == p) state = WIN_SCREEN
                else scoreFlash = 20
            }
            in (radius / 315.0f * 240.0f)..(radius / 315.0f * 250.0f) ->  {
                hitNotificationTimer = 15
                val p = getPoints(x, y)
                notification = HitNotification(p, HitNotification.Companion.HitType.DOUBLE)
                if(score > p) score -= p
                else if(score == p) state = WIN_SCREEN
                else scoreFlash = 20
            }
        }
    }

    private fun showHitNotification(c: Canvas) {
        notification?.let {
            val p = Paint()
            p.textAlign = Paint.Align.CENTER
            p.textSize = 40.0f
            p.style = Paint.Style.FILL
            p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
            c.drawRoundRect(mWidth / 2 - 100.0f, mHeight / 2 - 50.0f, mWidth / 2 + 100.0f, mHeight / 2 + 50.0f, 20f, 20f, p)
            p.color = when(it.type) {
                HitNotification.Companion.HitType.DOUBLE -> ResourcesCompat.getColor(context.resources, R.color.green, null)
                HitNotification.Companion.HitType.TRIPLE -> ResourcesCompat.getColor(context.resources, R.color.gold, null)
                HitNotification.Companion.HitType.MIDDLE -> ResourcesCompat.getColor(context.resources, R.color.gold, null)
                else -> Color.WHITE
            }
            p.style = Paint.Style.STROKE
            c.drawRoundRect(mWidth / 2 - 100.0f, mHeight / 2 - 50.0f, mWidth / 2 + 100.0f, mHeight / 2 + 50.0f, 20f, 20f, p)
            p.style = Paint.Style.FILL
            c.drawText(it.points.toString(), mWidth / 2.0f, mHeight / 2.0f, p)
        }
    }

    private fun getPoints(x: Int, y: Int) : Int {
        var a = Math.toDegrees(atan2(((mHeight / 2 - y).toDouble()), (mWidth / 2 - x ).toDouble()))
        when(a.toInt()) {
            in 0..90 -> a += 270
            in 91..180 -> a -= 90
            in 0 downTo -180 -> a += 270
        }
        return when(a.toInt()) {
            in 9..27 -> 1
            in 27..45 -> 18
            in 45..63 -> 4
            in 63..81 -> 13
            in 81..99 -> 6
            in 99..117 -> 10
            in 117..135 -> 15
            in 135..153 -> 2
            in 153..171 -> 17
            in 171..189 -> 3
            in 189..207 -> 19
            in 207..225 -> 7
            in 225..243 -> 16
            in 243..261 -> 8
            in 261..279 -> 11
            in 279..297 -> 14
            in 297..315 -> 9
            in 315..333 -> 12
            in 333..351 -> 2
            else -> 20
        }
    }

    private fun checkRestartPush(x: Float, y: Float) : Boolean {
        val restartSize = min(mWidth, mHeight) * 0.1f
        return (x in (mWidth - restartSize - 30)..(mWidth - 10.0f) && y in (mHeight - restartSize - 30)..(mHeight - 10.0f))
    }

    private fun restart() {
        dartPosition = DartState(mWidth / 2, mHeight - dart.height / 2, 180f, 1.0f,0.5f)
        score = 500
        darts.clear()
        state = GAME
    }
}