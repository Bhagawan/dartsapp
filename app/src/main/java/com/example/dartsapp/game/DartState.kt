package com.example.dartsapp.game

data class DartState(var xFromMiddle: Int, var yFromMiddle: Int, var angle: Float, var scaleX: Float, var scaleY: Float)
