package com.example.dartsapp.game

data class HitNotification(val points: Int, val type: HitType ) {
    companion object {
        enum class HitType {
            ORDINARY, DOUBLE, TRIPLE, MIDDLE
        }
    }
}
