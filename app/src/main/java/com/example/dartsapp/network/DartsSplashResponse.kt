package com.example.dartsapp.network

import androidx.annotation.Keep

@Keep
data class DartsSplashResponse(val url : String)